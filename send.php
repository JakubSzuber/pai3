<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "survey_db";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

// Assume user_id is 1, you can replace it with actual user id
$user_id = 1;

// Loop through all the questions
$final_answer = array();

for ($i = 1; $i <= 5; $i++) {
    if(isset($_POST['question11'.$i])) {
        $final_answer[$i] = $_POST['question11'.$i];
    }
    if(isset($_POST['question21'.$i])) {
        $final_answer[$i] = $_POST['question21'.$i];
    }
    if(isset($_POST['question31'.$i])) {
        $final_answer[$i] = $_POST['question31'.$i];
    }
    if(isset($_POST['question41'.$i])) {
        $final_answer[$i] = $_POST['question41'.$i];
    }
}

$query = "INSERT INTO answers (user_id, question_id, answer1, answer2, answer3, answer4, answer5) VALUES ('$user_id', '$i', '$final_answer[1]', '$final_answer[2]', '$final_answer[3]', '$final_answer[4]', '$final_answer[5]')";

if ($conn->query($query) === TRUE) {
  echo "New record created successfully";
} else {
  echo "Error: " . $query . "<br>" . $conn->error;
}


/*
$query = "INSERT INTO answers (user_id, question_id, answer1, answer2, answer3, answer4, answer5) VALUES ('1', '1', '$answer1', '$answer2', '$answer3', '$answer4', '$answer5')";
if ($conn->query($query) === TRUE) {
  echo "New record created successfully";
} else {
  echo "Error: " . $query . "<br>" . $conn->error;
}*/


$conn->close();
/*
CREATE TABLE IF NOT EXISTS `answers` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` int(11) NOT NULL,
	`question_id` int(11) NOT NULL,
	`answer1` text NOT NULL,
	`answer2` text NOT NULL,
	`answer3` text NOT NULL,
  `answer4` text NOT NULL,
	`answer5` text NOT NULL,
	PRIMARY KEY (`id`)
);
*/
?>
