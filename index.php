<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "survey_db";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

// Select data from the database
$sql = "SELECT id, content_question, sur_answer1, sur_answer2, sur_answer3, sur_answer4 FROM questions";
$result = $conn->query($sql);
?>

<form method="post" action="send.php">
<?php
if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    echo "<p>" . $row["content_question"]. "</p>";
    echo "<input type='radio' name='question1" . $row["id"]. "' value='" . $row["sur_answer1"] . "'>" . $row["sur_answer1"];
    echo "<input type='radio' name='question2" . $row["id"]. "' value='" . $row["sur_answer2"] . "'>" . $row["sur_answer2"];
    echo "<input type='radio' name='question3" . $row["id"]. "' value='" . $row["sur_answer3"] . "'>" . $row["sur_answer3"];
    echo "<input type='radio' name='question4" . $row["id"]. "' value='" . $row["sur_answer4"] . "'>" . $row["sur_answer4"];
  }
} else {
  echo "0 results";
}
$conn->close();
?>
<input type="submit" value="Submit">
</form>
