CREATE DATABASE IF NOT EXISTS `survey_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `survey_db`;

CREATE TABLE IF NOT EXISTS `questions` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
  `content_question` text NOT NULL,
	`sur_answer1` text NOT NULL,
	`sur_answer2` text NOT NULL,
	`sur_answer3` text NOT NULL,
	`sur_answer4` text NOT NULL,
	PRIMARY KEY (`id`)
);

INSERT INTO `questions` (`content_question`, `sur_answer1`, `sur_answer2`, `sur_answer3`, `sur_answer4`)
VALUES
('What is the your favorite food?', 'Pizza', 'Hotdog', 'Steak', 'Spaghetti'),
('What is your favorite color?', 'Red', 'Blue', 'Green', 'Yellow'),
('What is your favorite country?', 'Poland', 'Germany', 'France', 'Canada'),
('What is your favorite fish?', 'Pike', 'Catfish', 'Salmon', 'Trout'),
('What is your favorite animal?', 'Dog', 'Cat', 'Elephant', 'Lion');

CREATE TABLE IF NOT EXISTS `answers` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` int(11) NOT NULL,
	`question_id` int(11) NOT NULL,
	`answer1` text NOT NULL,
	`answer2` text NOT NULL,
	`answer3` text NOT NULL,
  `answer4` text NOT NULL,
	`answer5` text NOT NULL,
	PRIMARY KEY (`id`)
);

/*INSERT INTO `answers` (`user_id`, `question_id`, `answer1`, `answer2`, `answer3`, `answer4`, `answer5`)
VALUES (1, 1, 'Pizza', 'Red', 'Poland', 'Pike', 'Dog'),
VALUES (1, 1, 'Pizza', 'Red', 'Poland', 'Pike', 'Dog'),
VALUES (1, 1, 'Pizza', 'Red', 'Poland', 'Pike', 'Dog'),
VALUES (1, 1, 'Pizza', 'Red', 'Poland', 'Pike', 'Dog'),
VALUES (1, 1, 'Pizza', 'Red', 'Poland', 'Pike', 'Dog');
*/
